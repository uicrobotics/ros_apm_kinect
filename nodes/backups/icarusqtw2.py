#!/usr/bin/env python
import roslib; roslib.load_manifest('icarusqtw')
import rospy
from std_msgs.msg import String, Header
from sensor_msgs.msg import NavSatFix, NavSatStatus, Imu
import icarusqtw.msg
import sys,struct,time,os
import cv
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import String

sys.path.insert(0, os.path.join(os.path.dirname(os.path.realpath(__file__)), '/opt/ros/fuerte/share/mavlink/pymavlink'))


from optparse import OptionParser
parser = OptionParser("icarusqtw.py [options]")

parser.add_option("--baudrate", dest="baudrate", type='int',
                  help="master port baud rate", default=115200)
parser.add_option("--device", dest="device", default=None, help="serial device")
parser.add_option("--rate", dest="rate", default=10, type='int', help="requested stream rate")
parser.add_option("--source-system", dest='SOURCE_SYSTEM', type='int',
                  default=255, help='MAVLink source system for this GCS')
parser.add_option("--enable-control",dest="enable_control", default=False, help="Enable listning to control messages")

(opts, args) = parser.parse_args()

import mavutil

# create a mavlink serial instance
master = mavutil.mavlink_connection(opts.device, baud=opts.baudrate)

if opts.device is None:
    print("You must specify a serial device")
    sys.exit(1)

def wait_heartbeat(m):
    '''wait for a heartbeat so we know the target system IDs'''
    print("Waiting for APM heartbeat")
    m.wait_heartbeat()
    print("Heartbeat from APM (system %u component %u)" % (m.target_system, m.target_system))


def mav_control(roll,pitch,yaw,thrust):
    master.mav.set_roll_pitch_yaw_thrust_send(master.target_system, master.target_component,roll,pitch,yaw,thrust)

def send_rc(channels):
    master.mav.rc_channels_override_send(master.target_system, master.target_component,channels[0],channels[1],channels[2],channels[3],channels[4],channels[5],channels[6],channels[7])

#def send_state(Arm,Guided,Mode)
#master.mav.


pub_gps = rospy.Publisher('gps', NavSatFix)
#pub_imu = rospy.Publisher('imu', Imu)
pub_rc = rospy.Publisher('rc', icarusqtw.msg.RC)
pub_state = rospy.Publisher('state', icarusqtw.msg.State)
pub_vfr_hud = rospy.Publisher('vfr_hud', icarusqtw.msg.VFR_HUD)
pub_attitude = rospy.Publisher('attitude', icarusqtw.msg.Attitude)
pub_raw_imu =  rospy.Publisher('raw_imu', icarusqtw.msg.Mavlink_RAW_IMU)
rospy.Subscriber("send_rc", icarusqtw.msg.RC , send_rc)
rospy.Subscriber("control", icarusqtw.msg.Control , mav_control)


   
def initvariables():
    global x_center
    global y_center
    global RGB_CAMERA_WINDOW_WIDTH
    global RGB_CAMERA_WINDOW_HEIGHT
    global PITCH_SERVO_MAX
    global YAW_SERVO_MAX
    global distance
    x_center = -1
    y_center = -1
    distance = -1
    RGB_CAMERA_WINDOW_WIDTH = 640
    RGB_CAMERA_WINDOW_HEIGHT = 480
    PITCH_SERVO_MAX = 3.14
    YAW_SERVO_MAX = .78
 


gps_msg = NavSatFix()
#class depth_converter:

#  def __init__(self):
    #cv.NamedWindow("Depth Window",3)
#    self.bridge = CvBridge()
#    self.depth_sub = rospy.Subscriber("camera/depth/image",Image,self.callback)
#  def callback(self,data):
#    try:
#      depth_image = self.bridge.imgmsg_to_cv(data, "mono16")
      #print "Depth Stuff"
#      d = cv.Get2D(depth_image,100,100)
#      print d
      #cv.ShowImage("Depth Window",depth_image)
#      cv.WaitKey(10)
#    except CvBridgeError, e:
#      print e

class image_converter:

  def __init__(self):
    
    self.image_pub = rospy.Publisher("image_topic_2",Image)
    cv.NamedWindow("Color Track Window",1)

    #cv.NamedWindow("Blob Window",2)
    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber("camera/rgb/image_color",Image,self.callback)
    #self.depth_sub = rospy.Subscriber("camera/depth/image",Image,self.callbackDepth)
    #self.cv_depth = None
    

  def callbackDepth(self,data):
    try:
      global x_center
      global y_center
      global distance 
      self.cv_depth = self.bridge.imgmsg_to_cv(data)
      if x_center != -1:
        distance = self.cv_depth[int(y_center),int(x_center)]
        #distance = cv.Get2D(self.cv_depth,int(x_center),int(y_center))
      else:
        distance = -1
      #distance = d[1]
      #print d
      #cv.ShowImage("Depth Window",self.cv_depth) 
      cv.WaitKey(10)
    except CvBridgeError, e:
      print e
  def callback(self,data):
    try:
      
      cv_image = self.bridge.imgmsg_to_cv(data, "bgr8")
      #cv.Smooth(cv_image, cv_image, cv.CV_BLUR, 3); 
            
    #convert the image to hsv(Hue, Saturation, Value) so its  
    #easier to determine the color to track(hue) 
      hsv_img = cv.CreateImage(cv.GetSize(cv_image), 8, 3) 
      cv.CvtColor(cv_image, hsv_img, cv.CV_BGR2HSV) 
    
    #limit all pixels that don't match our criteria, in this case we are  
    #looking for purple but if you want you can adjust the first value in  
    #both turples which is the hue range(120,140).  OpenCV uses 0-180 as  
    #a hue range for the HSV color model 
      thresholded_img =  cv.CreateImage(cv.GetSize(hsv_img), 8, 1) 
      cv.InRangeS(hsv_img, (125, 80, 80), (128, 255, 255), thresholded_img) 
    
    #determine the objects moments and check that the area is large  
    #enough to be our object 
      thresholded_img = cv.GetMat(thresholded_img)
      moments = cv.Moments(thresholded_img) 
      area = cv.GetCentralMoment(moments, 0, 0) 
    
    #there can be noise in the video so ignore objects with small areas 
      if(area > 100000): 
        #determine the x and y coordinates of the center of the object 
        #we are tracking by dividing the 1, 0 and 0, 1 moments by the area 
        x = cv.GetSpatialMoment(moments, 1, 0)/area 
        y = cv.GetSpatialMoment(moments, 0, 1)/area 
        global x_center
        global y_center
        x_center = x
        y_center = y
        
    
       
        
        #create an overlay to mark the center of the tracked object 
        overlay = cv.CreateImage(cv.GetSize(cv_image), 8, 3) 
        
        cv.Circle(cv_image, (int(x), int(y)), 2, (255, 255, 255), 20) 
        #cv.Add(org_image, overlay, org_image) 
        #add the thresholded image back to the img so we can see what was  
        #left after it was applied 
        #cv.Merge(thresholded_img, None, None, None, cv_image) 
     
    #display the image  
      cv.ShowImage("Color Track Window", cv_image)
      #cv.ShowImage("Blob Window",thresholded_img)
      cv.WaitKey(10)
    except CvBridgeError, e:
      
      print e

    try:
      self.image_pub.publish(self.bridge.cv_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError, e:
      print e
def mainloop():
    initvariables()
    global RGB_CAMERA_WINDOW_HEIGHT
    global RGB_CAMERA_WINDOW_WIDTH
    global PITCH_SERVO_MAX
    global YAW_SERVO_MAX
    global distance
    #rospy.init_node('roscopter')
    rospy.init_node('image_converter', anonymous=True)
    ic = image_converter()
    #dc = depth_converter()
    i = 0
    j = 0
    direction = True
    mode = "Init"
    
    cur_pitch = 0
    cur_yaw = 0
    while not rospy.is_shutdown():
        
        rospy.sleep(0.001)
        msg = master.recv_match(blocking=False)
        if not msg:
            continue
        
	#DPG
        if mode == "Auto":
          pitch_set = (PITCH_SERVO_MAX/2)*(((2*y_center)/RGB_CAMERA_WINDOW_HEIGHT)-1)
          yaw_set = YAW_SERVO_MAX*(((2*x_center)/RGB_CAMERA_WINDOW_WIDTH)-1)
        else:
          pitch_set = 0
          yaw_set = 0
        print 'Mode: ' + mode + ' x: ' + str(int(x_center)) + ' y: ' + str(int(y_center)) + ' C_P_Angle: ' + str(int(cur_pitch*180/3.14159)) + ' S_P_Angle: ' + str(int(pitch_set*180/3.14159)) + ' C_Y_Angle: ' + str(int(cur_yaw)) + ' S_Y_Angle: ' + str(int(yaw_set*180/3.14))# + ' Dist: ' + str(distance)

	roll = 0
        thrust = 1
	i = i +1
	#Use this to send Roll, Pitch, Yaw, Thrust Commands.  Roll, Pitch, Yaw are in radians.  Thrust range from 0-1.  If no change is desired, set individual command to -1.
	
	if i == 300:
	  print("Changing Mode")
	  master.set_mode_auto()
	elif i < 300:
          mode = "Init"
	  print i
	  print msg.get_type()
	elif i > 300:
          mode = "Auto"
	  if direction:
            j = j + .005
          else:
            j = j - .005
          if j > .78:
            direction = False
          elif j < -.78:
            direction = True
          a = [j,j,j,j,j,j,j,j]
          #send_rc(a)
	  #print j
 	  mav_control(roll,pitch_set,yaw_set,thrust)


	  

	#Debugging
	#print("Roll Setpoint: %f Pitch Setpoint: %f Yaw Setpoint: %f Thrust Setpoint: %f " %(roll,pitch,yaw,thrust))
	#master.mav.set_mode_send(master.target_system, 
        if msg.get_type() == "BAD_DATA":
            if mavutil.all_printable(msg.data):
                sys.stdout.write(msg.data)
                sys.stdout.flush()
        else: 
            msg_type = msg.get_type()
            if msg_type == "RC_CHANNELS_RAW" :
                pub_rc.publish([msg.chan1_raw, msg.chan2_raw, msg.chan3_raw, msg.chan4_raw, msg.chan5_raw, msg.chan6_raw, msg.chan7_raw, msg.chan8_raw]) 
		#DPG
     	 	#print "Ch1: %d Ch2: %d Ch3: %d Ch4: %d Ch5: %d Ch6: %d Ch7: %d Ch8: %d" %(msg.chan1_raw, msg.chan2_raw, msg.chan3_raw, msg.chan4_raw, msg.chan5_raw, msg.chan6_raw, msg.chan7_raw, msg.chan8_raw)
            if msg_type == "HEARTBEAT":
                pub_state.publish(msg.base_mode & mavutil.mavlink.MAV_MODE_FLAG_SAFETY_ARMED, 
                                  msg.base_mode & mavutil.mavlink.MAV_MODE_FLAG_GUIDED_ENABLED, 
                                  mavutil.mode_string_v10(msg))
		#print msg.get_type()  #Print Heartbeat so we know the system is still responding.
            if msg_type == "VFR_HUD":
                pub_vfr_hud.publish(msg.airspeed, msg.groundspeed, msg.heading, msg.throttle, msg.alt, msg.climb)

            if msg_type == "GPS_RAW_INT":
                fix = NavSatStatus.STATUS_NO_FIX
                if msg.fix_type >=3:
                    fix=NavSatStatus.STATUS_FIX
                pub_gps.publish(NavSatFix(latitude = msg.lat/1e07,
                                          longitude = msg.lon/1e07,
                                          altitude = msg.alt/1e03,
                                          status = NavSatStatus(status=fix, service = NavSatStatus.SERVICE_GPS) 
                                          ))
            #pub.publish(String("MSG: %s"%msg))
            if msg_type == "ATTITUDE" :
                pub_attitude.publish(msg.roll, msg.pitch, msg.yaw, msg.rollspeed, msg.pitchspeed, msg.yawspeed)
		cur_pitch = msg.pitch
                cur_yaw = msg.yaw*180/3.14159

            #if msg_type == "LOCAL_POSITION_NED" :
                #print "Local Pos: (%f %f %f) , (%f %f %f)" %(msg.x, msg.y, msg.z, msg.vx, msg.vy, msg.vz)

            if msg_type == "RAW_IMU" :
                pub_raw_imu.publish (Header(), msg.time_usec, 
                                     msg.xacc, msg.yacc, msg.zacc, 
                                     msg.xgyro, msg.ygyro, msg.zgyro,
                                     msg.xmag, msg.ymag, msg.zmag)




# wait for the heartbeat msg to find the system ID
wait_heartbeat(master)


# waiting for 10 seconds for the system to be ready
print("Sleeping for 1 seconds to allow system, to be ready")
rospy.sleep(1)
print("Sending all stream request for rate %u" % opts.rate)
#for i in range(0, 3):

#master.mav.request_data_stream_send(master.target_system, master.target_component,
#                                    mavutil.mavlink.MAV_DATA_STREAM_ALL, opts.rate, 1)


if __name__ == '__main__':
    try:
        mainloop()
    except rospy.ROSInterruptException: pass
