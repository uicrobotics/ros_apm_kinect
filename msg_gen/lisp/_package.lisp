(cl:defpackage roscopter-msg
  (:use )
  (:export
   "<RC>"
   "RC"
   "<CONTROL>"
   "CONTROL"
   "<VFR_HUD>"
   "VFR_HUD"
   "<ATTITUDE>"
   "ATTITUDE"
   "<MAVLINK_RAW_IMU>"
   "MAVLINK_RAW_IMU"
   "<STATE>"
   "STATE"
  ))

